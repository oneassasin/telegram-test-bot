'use strict';

import Application from "@agiliumlabs/express-microservice";
import path from 'path';
import TelegramApi from "../telegram-api/index";

const config = process.env.APP_CONFIG ?
    require('./' + process.env.APP_CONFIG) :
    require('./config');

export default class TelegramApplication extends Application {

    start() {
        this.telegramApi = new TelegramApi();
        this.registerServices();
        this.telegramApi.initialize(this);
        var logPath = '../' + config.log4jsPath + '/telegram-bot.log';
        logPath = path.join(__dirname, logPath);
        super.start(config, logPath);
        let router = this.container.get('telegramRouter');
        router.telegramApi();
    }

    registerServices() {
        this.registerService('config', config);
    }
}
