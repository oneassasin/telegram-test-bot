'use strict';

require('babel-register')({
    ignore: /node_modules\/(?!@agiliumlabs)/,
    retainLines: true
});
require('./start');
