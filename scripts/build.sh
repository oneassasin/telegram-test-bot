#!/bin/bash

cd `dirname $0`/..

rm -rf node_modules &&
npm install &&
gulp u &&
gulp cs &&
if [ -d app/logs ]; then
  rm -rf app/logs
fi &&
if [ -d logs ]; then
  rm -rf logs
fi &&
rm -rf node_modules &&
npm install --production &&
if [ -d dist ]; then
  rm -rf dist
fi &&
tar --exclude=..* -czf /tmp/customers-api.tar.gz * .* &&
mkdir dist &&
cp /tmp/customers-api.tar.gz dist/customers-api.tar.gz
