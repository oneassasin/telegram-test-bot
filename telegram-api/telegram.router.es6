'use strict';

import Telegraf from 'telegraf';

/**
 * TelegramApi router service
 * @param {Telegraf} telegraf Instance of telegraf
 * @param {TelegrafWit} telegrafWit Instance of telegraf Wit middleware
 * @param {TelegramService} telegramService TelegramApi service
 * @return {TelegramRouter} telegramService router
 */
export default function telegramRouter(telegraf, telegrafWit, telegramService) {
    return new TelegramRouter(telegraf, telegrafWit, telegramService);
}
telegramRouter.$inject = ['telegraf', 'telegrafWit', 'telegramService'];

export class TelegramRouter {

    /**
     * Constructs telegram router
     * @param {Telegraf} telegraf Instance of telegraf
     * @param {TelegrafWit} telegrafWit Instance of telegraf Wit middleware
     * @param {TelegramService} telegramService TelegramApi service
     */
    constructor(telegraf, telegrafWit, telegramService) {
        this.telegraf = telegraf;
        this.telegrafWit = telegrafWit;
        this.telegramService = telegramService;
    }

    telegramApi() {
        this.telegraf.use(Telegraf.memorySession());
        this.telegraf.use(this.telegrafWit.middleware());
        this.telegrafWit.on('error', this.telegramService.onError);
        this.telegrafWit.on('weather', this.telegramService.onWeather);
        this.telegraf.startPolling();
    }

}
