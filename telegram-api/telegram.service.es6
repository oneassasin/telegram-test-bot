'use strict';

/**
 * Construct telegram service
 * @return {TelegramService}
 */
export default function telegramService() {
    return new TelegramService();
}

export class TelegramService {

    onError(context) {
        console.log('onError');
        console.log(context);
    }

    onWeather(context) {
        console.log('onWeather');
        context.wit.result = 'weather call result';
        return context.reply(context.wit.result);
    }

}
