'use strict';

import Telegraf from 'telegraf';

/**
 * Construct telegraf instance
 * @param config {Object} Configuration object
 * @return {Telegraf} Telegraf instance
 */
export default function telegrafService(config) {
    return new Telegraf(config.telegramApiKey);
}
telegrafService.$inject = ['config'];
