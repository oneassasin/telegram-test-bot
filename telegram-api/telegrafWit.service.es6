'use strict';

import TelegrafWit from 'telegraf-wit';

/**
 * Construct telegrafWit middleware
 * @param {Object} config Configuration object
 * @return {TelegrafWit}
 */
export default function telegrafService(config) {
    return new TelegrafWit(config.witApiKey);
}
telegrafService.$inject = ['config'];
