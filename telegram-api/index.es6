'use strict';

import telegrafService from './telegraf.service.es6';
import telegrafWitService from './telegrafWit.service.es6';
import telegramRouter from './telegram.router.es6';
import telegramService from './telegram.service.es6';

/**
 * Reusable TelegramApi
 */
export default class TelegramApi {

    /**
     * Initialize module
     * @param {Application} app express-microservice application
     */
    initialize(app) {
        if (this._init === true) {
            return;
        }
        app.registerService('telegraf', telegrafService);
        app.registerService('telegrafWit', telegrafWitService);
        app.registerService('telegramService', telegramService);
        app.registerService('telegramRouter', telegramRouter);
        this._init = true;
    }

}
